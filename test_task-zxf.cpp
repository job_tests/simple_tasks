
// https://interview.cups.online/live-coding

#include <iostream>

class A
{
public:
	A() { std::cout << "A()\n"; };
	virtual void foo() { std::cout << "A::foo()\n"; };
	virtual void bar() { std::cout << "A::bar()\n"; };
};

class B : public A
{
public:
	B() {
		std::cout << "B()\n";
		foo();
		bar();
	};
	void bar() { std::cout << "B::bar()\n"; };
};

class C : public B
{
public:
	C() { std::cout << "C()\n"; };
	void foo() { std::cout << "C::foo()\n"; };
	void bar() { std::cout << "C::bar()\n"; };
};

int main() {
	C x;
	return 0;
}

/* Вопрос: раннее или позднее связывание в конструкторе?
	A()
	B()
	A:foo()
	B::bar()
	C()
*/
// =============================================================================

#include <exception>
#include <iostream>

struct bomb_exception : public std::exception {};
struct main_exception : public std::exception {};

struct Bomb
{
	~Bomb() noexcept(false) { throw bomb_exception(); }
};

int main()
{
	try {
		Bomb bomb;
		throw main_exception();

	} catch(const std::exception &) {
		// обработчик 1
		std::cout << 1 << std::endl;
	} catch(const main_exception &) {
		// обработчик 2
		std::cout << 2 << std::endl;
	} catch(const bomb_exception &) {
		// обработчик 3
		std::cout << 3 << std::endl;
	}

	return 0;
}
/* Вопрос: Обратить внимание на порядок catch...
 1) Компилятор выдаст ошибку: exception of type 'const bomb_exception &' will be caught by earlier handler
 2) "terminating with uncaught exception of type bomb_exception: std::exception"
*/
// =============================================================================



// Написать "inplace" функцию удаляющую элементы с четными значениями
// {1,4,5} -> {1,5}
#include <algorithm>
void evenRemove(std::vector<int>& arr)
{
    //? std::erase_if(arr.begin(), arr.end(), [](int value) -> bool { return !(value & 1); })
}
// =============================================================================

/* Задача:
Given an array of integers nums which is sorted in ascending order, and an integer target,
write a function to search target in nums. If target exists, then return its index. Otherwise, return -1.
All the integers in nums are unique.

You must write an algorithm with O(log n) runtime complexity.

Example 1:

Input: nums = [-1,0,3,5,9,12], target = 9
Output: 4
Explanation: 9 exists in nums and its index is 4

Example 2:

Input: nums = [-1,0,3,5,9,12], target = 2
Output: -1
Explanation: 2 does not exist in nums so return -1
*/

int search(const std::vector<int>& nums, int target)
{
    //? auto it = std::lower_bound(nums.begin(), nums.end(), target);
    //? if(it == nums.end()) return -1;
    //? if(*it != target) return -1;
    //? return std::distance(nums.begin(), it);
}
